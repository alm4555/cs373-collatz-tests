#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz_cycle, collatz_max_cycle

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):

    # -----
    # cycle
    # -----

    def test_cycle_1(self):
        v = collatz_cycle(1)
        self.assertEqual(v, 1)
    
    def test_cycle_2(self):
        v = collatz_cycle(2)
        self.assertEqual(v, 2)

    def test_cycle_3(self):
        v = collatz_cycle(4)
        self.assertEqual(v, 3)
    
    def test_cycle_4(self):
        v = collatz_cycle(8)
        self.assertEqual(v, 4)
    
    def test_cycle_5(self):
        v = collatz_cycle(5)
        self.assertEqual(v, 6)

    # ---------
    # max_cycle
    # ---------

    def test_max_cycle_1(self):
        v = collatz_max_cycle(1, 10)
        self.assertEqual(v, 20)

    def test_max_cycle_2(self):
        v = collatz_max_cycle(100, 200)
        self.assertEqual(v, 125)

    def test_max_cycle_3(self):
        v = collatz_max_cycle(201, 210)
        self.assertEqual(v, 89)

    def test_max_cycle_4(self):
        v = collatz_max_cycle(900, 1000)
        self.assertEqual(v, 174)

    
    def test_max_cycle_5(self):
        v = collatz_max_cycle(7172, 14108)
        self.assertEqual(v, 276)



    # ----
    # read
    # ----

    def test_read1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read2(self):
        s = "10 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 1)

    def test_read3(self):
        s = "100 200"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 200)
    
    def test_read4(self):
        s = "200 100"
        i, j = collatz_read(s)
        self.assertEqual(i,  200)
        self.assertEqual(j, 100)
    
    def test_read5(self):
        s = "1 999999"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 999999)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    def test_eval_6(self):
        v = collatz_eval(14108, 7172)
        self.assertEqual(v, 276)
    
    def test_eval_7(self):
        v = collatz_eval(7172, 14108)
        self.assertEqual(v, 276)
    
    def test_eval_8(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)
    
    def test_eval_9(self):
        v = collatz_eval(1112, 2222)
        self.assertEqual(v, 182)
    
    def test_eval_10(self):
        v = collatz_eval(1112, 2223)
        self.assertEqual(v, 183)

    def test_eval_11(self):
        v = collatz_eval(1, 2000)
        self.assertEqual(v, 182)

    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")

    def test_print4(self):
        w = StringIO()
        collatz_print(w, 14108, 7172, 276)
        self.assertEqual(w.getvalue(), "14108 7172 276\n")

    def test_print5(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")
    
    def test_print6(self):
        w = StringIO()
        collatz_print(w, 1, 999999, 525)
        self.assertEqual(w.getvalue(), "1 999999 525\n")

    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve2(self):
        r = StringIO("1 10\n100 200\n\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve3(self):
        r = StringIO("10 1\n200 100\n\n210 201\n1000 900\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 1 20\n200 100 125\n210 201 89\n1000 900 174\n")

    def test_solve4(self):
        r = StringIO("14108 7172\n9417 16764\n\n7994 7818\n7076 2885\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "14108 7172 276\n9417 16764 276\n7994 7818 252\n7076 2885 262\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
