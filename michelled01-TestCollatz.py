#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # additional read tests #

    def test_read2(self):
        s = "10 20\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 10)
        self.assertEqual(j, 20)
        with self.assertRaises(IndexError):
            i, j = collatz_read("")
        
    def test_read3(self):
        s = "314 100\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 314)
        self.assertEqual(j, 100)
        with self.assertRaises(AttributeError):
            i, j = collatz_read(2)
        
    def test_read4(self):
        s = "420 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,    420)
        self.assertEqual(j, 999999)
        with self.assertRaises(TypeError):
            i, j = collatz_read("2 4" + 2)

    # ----
    # eval
    # ----
        
    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # additional eval tests #

    def test_eval_5(self):
        v = collatz_eval(7, 3)
        self.assertEqual(v, 17)
        with self.assertRaises(TypeError):
            v = collatz_eval(10)

    def test_eval_6(self):
        v = collatz_eval(22, 22)
        self.assertEqual(v, 16)
        with self.assertRaises(TypeError):
            v = collatz_eval("100 200")

    def test_eval_7(self):
        v = collatz_eval(999000, 999999)
        self.assertEqual(v, 396)
        with self.assertRaises(TypeError):
            v = collatz_eval((999000, 999999))

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # additional print tests #
    
    def test_print2(self):
        w = StringIO()
        collatz_print(w, 1, 101, 315)
        self.assertEqual(w.getvalue(), "1 101 315\n")
        with self.assertRaises(AttributeError):
            collatz_print(999, 1, 101, 315)
        
    def test_print3(self):
        w = StringIO()
        collatz_print(w, 1, 925, 2000)
        self.assertEqual(w.getvalue(), "1 925 2000\n")
        with self.assertRaises(AttributeError):
            collatz_print("", 1, 101, 315)
        
    def test_print4(self):
        w = StringIO()
        collatz_print(w, 1, 999888, 527)
        self.assertEqual(w.getvalue(), "1 999888 527\n")
        with self.assertRaises(AttributeError):
            collatz_print("ss", 1, 101, 315)

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    # additional solve tests #

    def test_solve2(self):
        r = StringIO("101 321\n157 201\n89 737\n9256 10000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "101 321 131\n157 201 125\n89 737 171\n9256 10000 260\n")
        with self.assertRaises(TypeError):
            collatz_solve(2, 4)

    def test_solve3(self):
        r = StringIO("3 5\n7 20\n216 256\n9843 10250\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "3 5 8\n7 20 21\n216 256 128\n9843 10250 242\n")
        with self.assertRaises(ValueError):
            collatz_solve("r", "w")

    def test_solve4(self):
        r = StringIO("7 8\n22 22\n194 216\n999000 999999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "7 8 17\n22 22 16\n194 216 120\n999000 999999 396\n")
        with self.assertRaises(TypeError):
            collatz_solve(2, "4")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out 
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
